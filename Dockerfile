# Initial solution
#FROM rust as builder
#WORKDIR app
#COPY . .
#RUN cargo build --release
#
#FROM rust as runtime
#WORKDIR app
#COPY --from=builder /app/target/release/epub-analyzer  /usr/local/bin
#ENTRYPOINT ["/usr/local/bin/epub-analyzer"]

# use cargo-chef
# https://www.lpalmieri.com/posts/fast-rust-docker-builds/
# build:
#     docker build -t epub-analyzer .
# run:
#     docker run -it --rm --name epub-analyzer epub-analyzer
FROM rust as planner
WORKDIR app
# We only pay the installation cost once, 
# it will be cached from the second build onwards
# To ensure a reproducible build consider pinning 
# the cargo-chef version with `--version X.X.X`
RUN cargo install cargo-chef 
COPY . .
RUN cargo chef prepare  --recipe-path recipe.json

FROM rust as cacher
WORKDIR app
RUN cargo install cargo-chef
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json

FROM rust as builder
WORKDIR app
COPY . .
# Copy over the cached dependencies
COPY --from=cacher /app/target target
COPY --from=cacher /usr/local/cargo /usr/local/cargo
RUN cargo build --release --bin epub-analyzer

FROM rust as runtime
WORKDIR app
COPY --from=builder /app/target/release/epub-analyzer /usr/local/bin
ENTRYPOINT ["/usr/local/bin/epub-analyzer"]
