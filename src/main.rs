// SPDX-License-Identifier: GPL-3.0-only
//
// epub-analyzer Authors: see AUTHORS.txt

use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::convert::TryInto;

use std::cmp::Ordering;
use std::path::Path;

use std::io::BufReader;

use epub::doc::EpubDoc;

///
#[derive(Eq, Debug)]
struct WordCount {
    count: u32,
    words: HashSet<String>,
}

impl<'a> WordCount {
    fn new(word: &String, count: u32) -> Self {
        let mut set = HashSet::new();
        set.insert(word.clone());
        Self { count, words: set }
    }

    fn append(&mut self, word: &String) {
        self.words.insert(word.clone());
    }

    fn rank(&self) -> u32 {
        self.count
    }
    fn word_count(&self) -> usize {
        self.words.len()
    }
    fn words(&'a self) -> &'a HashSet<String> {
        &self.words
    }
}

impl Ord for WordCount {
    fn cmp(&self, other: &Self) -> Ordering {
        self.count.cmp(&other.count)
    }
}

impl PartialOrd for WordCount {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl PartialEq for WordCount {
    fn eq(&self, other: &Self) -> bool {
        self.count == other.count
    }
}

///
enum InsertPos {
    Exist(usize),
    NotExist(usize),
}

///
struct MostCommonWords<'a> {
    word_bins: VecDeque<WordCount>,
    ignore_words: &'a HashSet<String>,
    max_word_bins: usize,
    min_word_length: usize,
}

impl<'a> MostCommonWords<'a> {
    fn new(max_word_bins: u32, min_word_length: u32, ignore_words: &'a HashSet<String>) -> Self {
        MostCommonWords {
            word_bins: VecDeque::with_capacity(max_word_bins.try_into().unwrap()),
            ignore_words: ignore_words,
            max_word_bins: max_word_bins.try_into().unwrap(),
            min_word_length: min_word_length.try_into().unwrap(),
        }
    }

    fn extract_from(&mut self, db: &HashMap<String, u32>) {
        db.iter().for_each(|pair| {
            self.add(pair.0, *pair.1);
        });
    }

    fn add(&mut self, word: &String, count: u32) {
        if word.len() < self.min_word_length {
            return;
        }
        if self.ignore_words.contains(word) {
            return;
        }
        match self.get_insert_index(count) {
            InsertPos::NotExist(index) => {
                self.word_bins.insert(index, WordCount::new(word, count));
            }
            InsertPos::Exist(index) => {
                let entry = self.word_bins.get_mut(index).unwrap();
                entry.append(word);
            }
        }
        if self.word_bins.len() > self.max_word_bins {
            self.word_bins.pop_back();
        }
    }

    fn get_insert_index(&self, value: u32) -> InsertPos {
        let mut i: usize = 0;
        for el in &self.word_bins {
            if value > el.rank() {
                break;
            }
            if value == el.rank() {
                return InsertPos::Exist(i);
            }
            i = i + 1;
        }
        i = i.min(self.word_bins.len());
        InsertPos::NotExist(i)
    }

    fn word_bin_count(&self) -> usize {
        self.word_bins.len()
    }

    fn max_count(&self) -> u32 {
        self.word_bins
            .front()
            .unwrap_or(&WordCount::new(&"".to_string(), 0))
            .rank()
    }

    fn min_count(&self) -> u32 {
        self.word_bins
            .back()
            .unwrap_or(&WordCount::new(&"".to_string(), u32::MAX))
            .rank()
    }

    fn word_count(&self) -> usize {
        self.word_bins
            .iter()
            .fold(0, |count, entry| count + entry.word_count())
    }

    fn print_most_common_words(&self) {
        println!("\nWORD STATS:");
        for el in &self.word_bins {
            println!("    {:?} : {:?}", el.rank(), el.words());
        }
    }
}

///
struct QueryDb {
    db: HashMap<String, u32>,
    ignore_words: HashSet<String>,
}

impl QueryDb {
    fn new(ignore_words: HashSet<String>) -> Self {
        QueryDb {
            db: HashMap::new(),
            ignore_words: ignore_words,
        }
    }

    fn add_word(&mut self, word: &String) {
        if let Some(count) = self.db.get_mut(&word.to_string()) {
            *count = *count + 1;
        } else {
            self.db.insert(word.to_string(), 1);
        }
    }

    fn print_statistics(&self) {
        let mut stats = MostCommonWords::new(30, 5, &self.ignore_words);
        stats.extract_from(&self.db);
        stats.print_most_common_words();
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    if std::env::args().len() != 2 {
        println!("{:?} <path-to-epub>", std::env::args().nth(0).unwrap())
    }

    let directory = std::env::args().nth(1).unwrap();
    let path = Path::new(&directory);

    if !path.is_dir() {
        println!("{:?} <path-to-epub>", std::env::args().nth(1).unwrap())
    }

    let mut ignore_words: HashSet<String> = HashSet::new();
    if std::env::args().len() == 3 {
        let ignore_file = std::env::args().nth(2).unwrap();
        let ignore_path = Path::new(&ignore_file);

        if ignore_path.is_file() {
            println!("ignore file {:?}", ignore_path);
            use std::fs::File;
            use std::io::BufRead;
            let file = File::open(ignore_path)?;
            let reader = BufReader::new(file);
            for line in reader.lines() {
                if let Ok(line) = line {
                    ignore_words.insert(line.trim().to_string());
                }
            }
        } else {
            println!("third parameter is optional, but should point to a file");
        }
    }

    println!("IGNORE: {:?}", &ignore_words);

    let mut query = QueryDb::new(ignore_words);

    for entry in std::fs::read_dir(directory)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            if let Some(filename) = path.to_str() {
                if !filename.ends_with(".epub") {
                    continue;
                }
                println!("Opening {:?}", filename);
                let mut doc = EpubDoc::new(filename)?;
                process_book(&mut doc, &mut query)?;
            }
        }
    }

    query.print_statistics();

    Ok(())
}

/// process epub ebook
fn process_book(doc: &mut EpubDoc, query: &mut QueryDb) -> Result<(), Box<dyn std::error::Error>> {
    println!(
        "Title      : {}",
        doc.mdata("title").expect("Missing title")
    );
    println!("Page count : {}", doc.get_num_pages());

    let m = doc.get_current_mime()?;
    assert_eq!("application/xhtml+xml", m);

    loop {
        //println!("Processing chapter #{:?}", doc.get_current_id());
        match process_chapter(&doc.get_current_str()?, query) {
            Err(e) => println!("Err: {:?}", e),
            _ => {}
        }
        match doc.go_next() {
            Ok(()) => {}
            _ => {
                break;
            }
        }
    }

    Ok(())
}

/// process single chapter of an ebook
fn process_chapter(xml: &str, query: &mut QueryDb) -> Result<(), Box<dyn std::error::Error>> {
    let doc = roxmltree::Document::parse(xml)?;
    let elem = doc
        .descendants()
        .find(|n| n.tag_name().name() == "body")
        .expect("body tag");
    assert!(elem.has_tag_name("body"));
    process_chapter_body(&elem, query);
    Ok(())
}

/// process the body tag of an ebook chapter
fn process_chapter_body(xml_node: &roxmltree::Node, query: &mut QueryDb) {
    process_element_text(&xml_node, query);
    xml_node
        .children()
        .for_each(|node| process_chapter_body(&node, query));
}

/// trim whitespace, convert to lowercase, and split into single words
fn process_element_text<'a>(xml_node: &'a roxmltree::Node, query: &mut QueryDb) {
    let text = xml_node.text().unwrap_or("");
    text.trim()
        .to_lowercase()
        .split_terminator(|c: char| c.is_ascii_punctuation() || c.is_whitespace())
        .filter(|vec| !vec.is_empty())
        .for_each(|word| {
            query.add_word(&word.to_string());
        });
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_word_count_append() {
        let mut count = WordCount::new(&"word1".to_string(), 7);
        assert_eq!(7, count.rank());
        assert_eq!(1, count.word_count());
        count.append(&"word2".to_string());
        assert_eq!(7, count.rank());
        assert_eq!(2, count.word_count());
        count.append(&"word1".to_string());
        assert_eq!(7, count.rank());
        assert_eq!(2, count.word_count());
    }

    #[test]
    fn test_word_count_cmp() {
        let count1 = WordCount::new(&"word1".to_string(), 7);
        let count2 = WordCount::new(&"word2".to_string(), 6);
        let count3 = WordCount::new(&"word3".to_string(), 7);
        assert_eq!(count1, count3);
        assert_ne!(count1, count2);
        assert_ne!(count3, count2);
        assert!(count1 > count2);
        assert!(count3 > count2);
        assert!(count2 <= count1);
        assert!(count2 <= count3);
    }

    #[test]
    fn test_most_common_words_insert_pos() {
        let ignore_words = HashSet::new();
        let mut most_common_words = MostCommonWords::new(2, 1, &ignore_words);

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(5) {
            assert_eq!(0, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        most_common_words.add(&"Word5.1".to_string(), 5);

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(6) {
            assert_eq!(0, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::Exist(i) = most_common_words.get_insert_index(5) {
            assert_eq!(0, i, "Insert position exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(4) {
            assert_eq!(1, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        most_common_words.add(&"Word3.1".to_string(), 3);

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(6) {
            assert_eq!(0, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::Exist(i) = most_common_words.get_insert_index(5) {
            assert_eq!(0, i, "Insert position exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(4) {
            assert_eq!(1, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::Exist(i) = most_common_words.get_insert_index(3) {
            assert_eq!(1, i, "Insert position exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        most_common_words.add(&"Word4.1".to_string(), 4);

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(6) {
            assert_eq!(0, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::Exist(i) = most_common_words.get_insert_index(5) {
            assert_eq!(0, i, "Insert position exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::Exist(i) = most_common_words.get_insert_index(4) {
            assert_eq!(1, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(3) {
            assert_eq!(2, i, "Insert position exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        most_common_words.add(&"Word6.1".to_string(), 6);

        if let InsertPos::Exist(i) = most_common_words.get_insert_index(6) {
            assert_eq!(0, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::Exist(i) = most_common_words.get_insert_index(5) {
            assert_eq!(1, i, "Insert position exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(4) {
            assert_eq!(2, i, "Insert position not exist");
        } else {
            assert!(false, "Insert position mismatch");
        }

        if let InsertPos::NotExist(i) = most_common_words.get_insert_index(3) {
            assert_eq!(2, i, "Insert position exist");
        } else {
            assert!(false, "Insert position mismatch");
        }
    }

    #[test]
    fn test_most_common_words_add() {
        let ignore_words = HashSet::new();
        let mut most_common_words = MostCommonWords::new(2, 1, &ignore_words);

        most_common_words.add(&"Word5.1".to_string(), 5);
        assert_eq!(1, most_common_words.word_bin_count());
        assert_eq!(5, most_common_words.max_count());
        assert_eq!(5, most_common_words.min_count());
        assert_eq!(1, most_common_words.word_count());

        most_common_words.add(&"Word3.1".to_string(), 3);
        assert_eq!(2, most_common_words.word_bin_count());
        assert_eq!(5, most_common_words.max_count());
        assert_eq!(3, most_common_words.min_count());
        assert_eq!(2, most_common_words.word_count());

        most_common_words.add(&"Word6.1".to_string(), 6);
        assert_eq!(2, most_common_words.word_bin_count());
        assert_eq!(6, most_common_words.max_count());
        assert_eq!(5, most_common_words.min_count());
        assert_eq!(2, most_common_words.word_count());

        most_common_words.add(&"Word3.2".to_string(), 3);
        assert_eq!(2, most_common_words.word_bin_count());
        assert_eq!(6, most_common_words.max_count());
        assert_eq!(5, most_common_words.min_count());
        assert_eq!(2, most_common_words.word_count());

        most_common_words.add(&"Word5.2".to_string(), 5);
        assert_eq!(2, most_common_words.word_bin_count());
        assert_eq!(6, most_common_words.max_count());
        assert_eq!(5, most_common_words.min_count());
        assert_eq!(3, most_common_words.word_count());

        most_common_words.add(&"Word7.1".to_string(), 7);
        assert_eq!(2, most_common_words.word_bin_count());
        assert_eq!(7, most_common_words.max_count());
        assert_eq!(6, most_common_words.min_count());
        assert_eq!(2, most_common_words.word_count());
    }
}
