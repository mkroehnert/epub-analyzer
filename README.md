# epub-analyzer

Analyze all epub ebooks stored inside a directory and print the most occuring words.

The directory is specified as the first commandline argument.

A file containing words to ignore can optionally be specified as second parameter.
Each word is specified on a new line.

usage: cargo run -- /path/to/epub-dir /path/to/ignore-words.txt

Sourcecode (GPL-3.0) available here: https://mkroehnert.gitlab.io/epub-analyzer

# Used Open Source Libraries #

* [epub (GPL-3.0)](https://crates.io/crates/epub)
* [roxmltree (MIT/Apache-2.0)](https://crates.io/crates/roxmltree)

# Authors

See [AUTHORS](AUTHORS.txt) for list of WeightRS authors and contributors

# License

The source code of this application is available under the GPL-3.0 license.
See [LICENSE](LICENSE) for details.

Copyright (C) 2020 Manfred Kröhnert

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
